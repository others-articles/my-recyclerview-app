package com.example.myrecyclerviewapp.Views

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.IdRes
import androidx.recyclerview.widget.RecyclerView
import android.graphics.BitmapFactory
import com.example.myrecyclerviewapp.Models.MyObject


class MyViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {


    private val viewMap: MutableMap<Int, View> = HashMap()

    init {
        findViewItems(itemView)
    }

    private fun findViewItems(itemView: View) {
    addToMap(itemView)
    if (itemView is ViewGroup) {
        val childCount = itemView.childCount
        (0 until childCount)
            .map { itemView.getChildAt(it) }
            .forEach { findViewItems(it) }
    }
    }

    private fun addToMap(itemView: View) {
    viewMap[itemView.id] = itemView
    }

    fun setHeader(@IdRes id: Int, text: String) {

        val view = (viewMap[id] ?: throw IllegalArgumentException("View for $id not found")) as? TextView ?: throw IllegalArgumentException("View for $id is not a TextView")
        view.text = text

    }

    fun setItems(item: MyObject, @IdRes textViewId: Int,@IdRes imageViewId: Int) {


        val imageView = (viewMap[imageViewId] ?: throw IllegalArgumentException("View for $imageViewId not found")) as ImageView
        val imageName = item.imageName
        val res = itemView.context.resources
        val packageName = imageView.context.packageName
        val resID = res.getIdentifier(imageName, "drawable", packageName)
        val bm = BitmapFactory.decodeResource(res,resID)
        imageView.setImageBitmap(bm)


        val view = (viewMap[textViewId] ?: throw IllegalArgumentException("View for $textViewId not found")) as? TextView ?: throw IllegalArgumentException("View for $textViewId is not a TextView")
        view.text = item.text

    }
}