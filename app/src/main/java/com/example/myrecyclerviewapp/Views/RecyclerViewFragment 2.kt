package com.example.myrecyclerviewapp.Views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myrecyclerviewapp.Managers.MyAdapter
import com.example.myrecyclerviewapp.Models.MyObject
import com.example.myrecyclerviewapp.Models.RecyclerViewContainer
import com.example.myrecyclerviewapp.R
import kotlinx.android.synthetic.main.recyclerview_fragment.*

class RecyclerViewFragment: Fragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var viewAdapter: RecyclerView.Adapter<*>

    private var itemList = mutableListOf<RecyclerViewContainer>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val viewOfLayout = inflater.inflate(R.layout.recyclerview_fragment, container, false)
        return viewOfLayout
    }

    companion object {
        fun newInstance(): RecyclerViewFragment = RecyclerViewFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setData()
        setRecyclerView()
    }

    private fun setData() { // please use loop statement for your dataset, this is just a sample of how to manage very simple data.

        val object1 = MyObject("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", "image1")
        val object2 = MyObject("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", "image2")

        val item1 = RecyclerViewContainer(null, true, "Title 1")
        val item2 = RecyclerViewContainer(object1, false, null)
        val item3 = RecyclerViewContainer(object1, false, null)
        val item4 = RecyclerViewContainer(null, true, "Title 2")
        val item5 = RecyclerViewContainer(object2, false, null)
        val item6 = RecyclerViewContainer(object2, false, null)


        itemList.add(item1)
        itemList.add(item2)
        itemList.add(item3)
        itemList.add(item4)
        itemList.add(item5)
        itemList.add(item6)
    }

    private fun setRecyclerView() {

        viewManager = LinearLayoutManager(this.context)
        val myAdapter = MyAdapter(itemList)

        recyclerView = recycler_view.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = myAdapter
            visibility = View.VISIBLE
        }

    }
}