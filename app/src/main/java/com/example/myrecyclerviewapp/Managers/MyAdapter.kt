package com.example.myrecyclerviewapp.Managers

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myrecyclerviewapp.Models.RecyclerViewContainer
import com.example.myrecyclerviewapp.R
import com.example.myrecyclerviewapp.Views.MyViewHolder

enum class RowType {
    HEADER,
    ROW
}

class MyAdapter(private val dataset: MutableList<RecyclerViewContainer>) : RecyclerView.Adapter<MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)

        val inflatedView : View = when (viewType) {
            RowType.ROW.ordinal -> layoutInflater.inflate(R.layout.row_view, parent,false)
            else -> layoutInflater.inflate(R.layout.header_view, parent,false)
        }
        return MyViewHolder(inflatedView)
    }

    override fun getItemCount() = dataset.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val item = dataset[position]

        if (item.isHeader) {
            item.headerTitle?.let { holder.setHeader(R.id.header_text_view, item.headerTitle!!) }
        }else {
            holder.setItems(item.myObject!!, R.id.text_view, R.id.image_view)
        }

        holder.itemView.setOnClickListener {
            println("Pressed at row $position")
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (dataset[position].isHeader) {
            return 0
        }else {
            return 1
        }
    }

}
