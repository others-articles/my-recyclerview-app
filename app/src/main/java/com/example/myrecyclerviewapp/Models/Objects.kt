package com.example.myrecyclerviewapp.Models

class MyObject(text: String, imageName: String) {
    val text = text
    val imageName = imageName
}

class RecyclerViewContainer (var myObject: MyObject?, var isHeader: Boolean, var headerTitle: String?)

